<?php

declare(strict_types=1);

namespace Codart\ColorCMS\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class Base
 * @package Codart\ColorCMS\Controller
 *
 * @author Artur Szymczyk <arturszymczyk1@gmail.com>
 */
abstract class Base extends AbstractController
{}